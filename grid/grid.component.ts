import {Component, Input} from '@angular/core';

import {Grid} from './grid';

@Component({
    selector: 'ggs-grid',
    styleUrls: ['./grid.scss'],
    templateUrl: './grid.html',
})
export class GridComponent {
    @Input()
    grid: Grid;
}
