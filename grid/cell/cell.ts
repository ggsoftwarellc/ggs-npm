export abstract class Cell {
    sortProperty: string = 'data';
    sortTransform: (input: any) => any;

    data: any;
    opts: {[value: string]: any} = {};
}