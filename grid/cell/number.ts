import {Component} from '@angular/core';

import {Cell} from './cell';

@Component({
    template: '{{ data }}',
})
export class NumberCell extends Cell {
    ngOnInit() {
        this.data = parseFloat(this.data);

        if (this.opts['fixed'] > 0) {
            this.data = this.data.toFixed(this.opts['fixed']);
        }
    }
}