import {Component} from '@angular/core';

import {Cell} from './cell';

@Component({
    template: '{{ data }}',
})
export class StringCell extends Cell {
}