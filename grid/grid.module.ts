import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';

import {NumberCell, StringCell} from './cell';
import {ColumnComponent} from './column.component';
import {GridComponent} from './grid.component';

@NgModule({
    declarations: [
        ColumnComponent,
        GridComponent,
        NumberCell,
        StringCell,
    ],
    entryComponents: [
        NumberCell,
        StringCell,
    ],
    exports: [
        GridComponent,
    ],
    imports: [
        CommonModule,
    ]
})
export class GridModule {
}