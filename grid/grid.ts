import 'rxjs/operator/throttle';

import {Observable} from 'rxjs/Observable';

import {Cell} from './cell';
import {Column} from './column';

export interface GridData { [id: string]: any }

export interface ColumnDef {
    id: string;
    title: string;
    cell: any;
    opts?: {[key: string]: any};
}

export interface GridOptions {
    cols: ColumnDef[];
    data?: GridData[];
    dataFormatter?: (row: GridData, index: number) => void;
    sortBy?: number;
    sortable?: boolean;
    sortAsc?: boolean;
    perPage?: number;

    onPage?: (grid: Grid, page: number) => Observable<any[]>;
}

export class Grid {
    private cols: Column[] = [];
    private data: GridData[] = [];
    private dataFormatter: (row: GridData, index: number) => void;

    private loaded: boolean = false;

    private sortColumn: Column;
    private sortAsc: boolean;
    private sortable: boolean;

    private curPage: number;

    perPage: number;
    total: number;

    private onPage: (grid: Grid, page: number) => Observable<GridData[]>;

    constructor(opts: GridOptions) {
        for (let i = 0; i < opts.cols.length; i++) {
            let col = opts.cols[i];
            this.cols.push(new Column(col.id, col.title, col.cell, col.opts));
        }

        if (opts.data) {
            this.data = opts.data;
        }

        this.dataFormatter = opts.dataFormatter;
        this.sortColumn = this.cols[opts.sortBy ? opts.sortBy : 0];
        this.sortAsc = typeof(opts.sortAsc) == 'undefined' ? true : opts.sortAsc;
        this.sortable = typeof(opts.sortable) == 'undefined' ? true : opts.sortable;
        this.perPage = typeof(opts.perPage) == 'undefined' ? 0 : opts.perPage;
        this.curPage = 0;

        this.onPage = opts.onPage;

        this.next();
    }

    sort(column: Column) {
        if (!this.sortable) {
            return;
        }

        if (this.sortColumn == column) {
            this.sortAsc = !this.sortAsc;
        }

        this.sortColumn = column;
        this.sortData();
    }

    clear() {
        this.data = [];
        this.loaded = false;
    }

    add(data: {[id: string]: any}) {
        this.data.push(data);
    }

    rows(): {[id: string]: any}[] {
        this.init();

        if (this.perPage > 0) {
            return this.data.slice(this.curPage * this.perPage, this.curPage * this.perPage + this.perPage);
        }

        return this.data;
    }

    count(): number {
        return this.data.length;
    }

    paged(): boolean {
        return this.perPage > 0 ? true : false;
    }

    cur(): number {
        return this.curPage;
    }

    pages(): number {
        let total = this.total ? this.total : this.data.length;

        return Math.ceil(total / this.perPage);
    }

    next() {
        if (this.curPage * this.perPage + this.perPage + 1 < this.pages()) {
            this.curPage++;
        }

        this.loadPage();
    }

    prev() {
        if (this.curPage >= 1) {
            this.curPage--;
        }

        this.loadPage();
    }

    goto(page: number) {
        this.curPage = page;
    }

    private init() {
        if (this.loaded) {
            return;
        }

        if (this.dataFormatter) {
            for (var i = 0; i < this.data.length; i++) {
                this.dataFormatter(this.data[i], i);
            }
        }

        if (this.sortable) {
            this.sortData();
        }

        this.loaded = true;
    }

    private loadPage() {
        if (!this.onPage) {
            return;
        }

        console.debug('load page');

        this.onPage(this, this.curPage).subscribe((rows: GridData[]) => {
            if (rows.length <= 0) {
                console.debug('no row data, skipping');
                return;
            }

            console.debug('observed new page');

            this.clear();
            this.data = rows;
            this.init();

            console.debug('page load complete');
        });
    }

    private sortData() {
        if (this.data.length == 0) {
            return;
        }

        this.data = this.data.sort((a, b) => {
            let sortProperty = this.sortColumn.cell.sortProperty;

            if (!sortProperty) {
                sortProperty = this.sortColumn.id;
            }

            let ad = a[this.sortColumn.id][sortProperty];
            let bd = b[this.sortColumn.id][sortProperty];

            let transform = this.sortColumn.cell.sortTransform;
            if (transform) {
                ad = transform(ad);
                bd = transform(bd);
            }

            if (typeof(ad) == 'number') {
                return this.sortAsc ? bd < ad ? 1 : -1 : ad < bd ? 1 : -1;
            } else {
                return this.sortAsc ? ad.localeCompare(bd) : bd.localeCompare(ad);
            }
        });
    }
}
