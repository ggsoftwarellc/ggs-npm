import {Cell} from './cell';

export class Column {
    id: string;
    name: string;
    cell: Cell;
    opts: {[key: string]: any};

    constructor(id: string, name: string, cell: Cell, opts: {[key: string]: any}) {
        this.id = id;
        this.name = name;
        this.cell = cell;
        this.opts = opts;
    }
}