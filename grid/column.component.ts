import {Component, ComponentFactoryResolver, ComponentRef, Input, Type, ViewChild, ViewContainerRef} from '@angular/core';

import {Cell} from './cell';
import {Column} from './column';

@Component({
    selector: '[grid-column]',
    template: '<span #target></span>',
})
export class ColumnComponent {
    @ViewChild('target', {read: ViewContainerRef}) target;

    @Input('grid-column')
    column: Column;
    @Input('grid-data')
    data: any;

    private isViewInitialized: boolean = false;
    private cmpRef: ComponentRef<any>;

    constructor(private componentFactoryResolver: ComponentFactoryResolver) {
    }

    updateComponent() {
        if (!this.isViewInitialized) {
            return;
        }

        if (this.cmpRef) {
            this.cmpRef.destroy();
        }

        // todo: figure out how to remove this "as any" cast
        this.cmpRef = this.appendNextToLocation<Cell>(this.column.cell as any, this.target);
        if (this.cmpRef) {
            if (this.cmpRef.instance instanceof Cell) {
                this.cmpRef.instance.data = this.data;

                if (this.column.opts) {
                    this.cmpRef.instance.opts = this.column.opts;
                }
            } else {
                console.error('component reference is not an instance of Cell');
                console.log(this.column.cell);
            }
        } else {
            console.error('failed to create component reference');
            console.log(this.column.cell);
        }
    }

    appendNextToLocation<T>(ComponentClass: Type<T>, location: ViewContainerRef): ComponentRef<T> {
        let componentFactory = this.componentFactoryResolver.resolveComponentFactory(ComponentClass);
        let parentInjector = location.parentInjector;

        return location.createComponent(componentFactory, location.length);
    }

    ngOnChanges() {
        this.updateComponent();
    }

    ngAfterContentInit() {
        this.isViewInitialized = true;
        this.updateComponent();
    }

    ngOnDestroy() {
        if (this.cmpRef) {
            this.cmpRef.destroy();
        }
    }
}