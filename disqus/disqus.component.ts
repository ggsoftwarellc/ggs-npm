import { DisqusService } from './disqus.service';
import { ChangeDetectionStrategy, Component, Input, OnChanges, Renderer, SimpleChanges } from '@angular/core';

@Component({
    selector: 'ggs-disqus',
    template: '<div id="disqus_thread"></div>',
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class DisqusComponent implements OnChanges {
    @Input()
    shortname: string;

    @Input()
    identifier: string;

    @Input()
    url: string;

    @Input()
    categoryId: string;

    @Input()
    lang: string;

    @Input()
    title: string;

    private loaded = false;

    constructor(private renderer: Renderer, private dService: DisqusService) {
    }

    ngOnChanges(changes: SimpleChanges) {
        this.dService.reset(this.renderer, this.getConfig());
    }

    getConfig() {
        return {
            url: this.validatedUrl(),
            identifier: this.identifier,
            categoryId: this.categoryId,
            title: this.title,
            language: this.lang
        };
    }

    validatedUrl() {
        if (this.url) {
            const r = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;

            if (r.test(this.url)) {
                return this.url;
            }
        }
        return this.dService.url;
    }
}
