import { CoreModule } from '../core/core.module';
import { DisqusComponent } from './disqus.component';
import { DisqusService } from './disqus.service';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

@NgModule({
    declarations: [
        DisqusComponent,
    ],
    exports: [
        DisqusComponent,
    ],
    providers: [
        DisqusService,
    ],
    imports: [
        CommonModule,
        CoreModule,
    ]
})
export class DisqusModule {
}