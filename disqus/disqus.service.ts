import { WindowRef } from '../core/window-ref';
import { Injectable, Renderer } from '@angular/core';
import { Observable } from 'rxjs/Observable';

declare const global: any; // To make AoT compiler (ngc) happy

@Injectable()
export class DisqusService {
    public static shortName: string;
    
    comments$: Observable<void>;
    counts$: Observable<void>;

    constructor(private winRef: WindowRef) {
    }

    get url(): string {
        return (this.window) ? this.window.location.href : (<any>global).url || '';
    }

    get window(): any {
        return this.winRef.nativeWindow;
    }

    get disqus(): any {
        return (this.window) ? this.window.DISQUS : (<any>global).DISQUS;
    }

    get disqusWidgets(): any {
        return (this.window) ? this.window.DISQUSWIDGETS : (<any>global).DISQUSWIDGETS;
    }

    getCounts(renderer: Renderer) {
        this.initCounts(renderer).subscribe(() => {
            this.disqusWidgets.getCount({ reset: true });
        });
    }

    reset(renderer: Renderer, config: any) {
        this.initComments(renderer).subscribe(() => {
            this.disqus.reset({
                reload: true,
                config: this.getConfigCallback(config)
            });
        });
    }

    private getConfigCallback(config: any) {
        return function () {
            this.page.category_id = config.categoryId;
            this.page.identifier = config.identifier;
            this.page.title = config.title;
            this.page.url = config.url;

            this.language = config.language;
        };
    }

    private initComments(renderer: Renderer): Observable<void> {
        if (!this.comments$) {
            this.comments$ = new Observable<void>(o => {
                const diqusScript = renderer.createElement(this.window.document.body, 'script');
                diqusScript.src = `//${DisqusService.shortName}.disqus.com/embed.js`;
                diqusScript.async = true;
                diqusScript.type = 'text/javascript';
                renderer.setElementAttribute(diqusScript, 'data-timestamp', new Date().getTime().toString());

                const i = setInterval(() => {
                    if (this.disqus) {
                        clearInterval(i);

                        o.next();
                        o.complete();
                    }
                }, 50);
            }).publishReplay().refCount();
        }

        return this.comments$;
    }

    private initCounts(renderer: Renderer): Observable<void> {
        if (!this.counts$) {
            this.counts$ = new Observable<void>(o => {
                const countScript = renderer.createElement(this.window.document.body, 'script');
                countScript.src = `//${DisqusService.shortName}.disqus.com/count.js`;
                countScript.async = true;
                countScript.type = 'text/javascript';
                renderer.setElementAttribute(countScript, 'id', 'dsq-count-scr');

                const i = setInterval(() => {
                    if (this.disqusWidgets) {
                        clearInterval(i);

                        o.next();
                        o.complete();
                    }
                }, 50);
            }).publishReplay().refCount();
        }
        return this.counts$;
    }
}
