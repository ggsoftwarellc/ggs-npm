export let tags: {[key: string]: any} = {
    adsense: {
        'leaderboard-atf': '2736854655',
        'leaderboard-btf': '8559938652',
        'medrec-atf': '',
        'medrec-btf': '',
        'mobile-leaderboard-atf': '5550720259',
        'mobile-leaderboard-btf': '7163930652',
    },
    dfp: {
        'leaderboard-atf': [728, 90],
        'leaderboard-btf': [728, 90],
        'medrec-atf': [300, 250],
        'medrec-btf': [300, 250],
        'mobile-leaderboard-atf': [320, 50],
        'mobile-leaderboard-btf': [320, 50],
    }
};