import {Router} from '@angular/router';
import {Observable} from 'rxjs';

import {AdConfig} from './ad.config';
import {tags} from './tags';

let adsBlocked: boolean = null;
let adsBlocked$: Observable<boolean> = null;
let dfpTagsLoaded: boolean = false;
let dfpLastRefresh;

export function initBlocked(): Observable<boolean> {
    if (adsBlocked$ == null) {
        console.debug('checking if ads are blocked');

        adsBlocked$ = new Observable<boolean>(o => {
            let testAd = document.createElement('div');
            testAd.innerHTML = '&nbsp;';
            testAd.className = 'adsbox';

            document.body.appendChild(testAd);

            setTimeout(() => {
                adsBlocked = testAd.offsetHeight === 0;
                document.body.removeChild(testAd);

                console.debug(`ad blocking is: ${adsBlocked ? 'enabled' : 'disabled'}`);
                o.next(adsBlocked);
                o.complete();
            }, 250);
        });
    }

    return adsBlocked$;
}

export function initDFPTags(router: Router): void {
    if (dfpTagsLoaded) {
        return;
    }
    console.debug(`ggs-ad: init dfp tags`);
    dfpTagsLoaded = true;

    let googletag = window['googletag'] || {};
    googletag.cmd = googletag.cmd || [];
    googletag.cmd.push(() => {
        Object.keys(tags['dfp']).forEach(tag => {
            console.debug(`  /308365556/${tag}:${JSON.stringify(tags['dfp'][tag])}`);
            googletag.defineSlot(`/308365556/${tag}`, tags['dfp'][tag], tag).addService(googletag.pubads());
        });
    });

    googletag.pubads().enableSingleRequest();
    googletag.enableServices();

    if (AdConfig.refresh) {
        dfpLastRefresh = +new Date();

        setInterval(() => {
            if ((+new Date()) > dfpLastRefresh + (AdConfig.refresh * 1000)) {
                dfpLastRefresh = +new Date();
                console.debug(`refresh ads`);
                googletag.pubads().refresh();
            }
        }, 1000);

        router.events.subscribe(event => {
            dfpLastRefresh = +new Date();
        });
    }
}