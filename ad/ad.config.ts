export class AdConfig {
    static enabled: boolean;
    static siteName: string;
    static routerLink: string[];
    static refresh: number;
}