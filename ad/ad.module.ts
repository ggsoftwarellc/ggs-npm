import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

import {AdComponent} from './ad.component';

@NgModule({
    declarations: [
        AdComponent,
    ],
    exports: [
        AdComponent,
    ],
    imports: [
        CommonModule,
        RouterModule,
    ]
})
export class AdModule {
}