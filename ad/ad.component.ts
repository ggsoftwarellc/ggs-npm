import {Component, ElementRef, Input, Renderer, ViewChild} from '@angular/core';
import {Router} from '@angular/router';

import {AdConfig} from './ad.config';
import {initBlocked, initDFPTags} from './functions';
import {tags} from './tags';

@Component({
    selector: 'ggs-ad',
    templateUrl: './ad.html',
    styleUrls: ['./ad.scss'],
})
export class AdComponent {
    @ViewChild('adElement')
    adElement: ElementRef;

    // atf, btf, mid, etc.
    @Input()
    format: string;

    // leaderboard, medrec, etc.
    @Input()
    type: string;

    config: AdConfig;

    zone;
    validSizes = [];
    provider: string = 'dfp';
    enabled: boolean = true;
    showWhitelist: boolean = false;
    link: any;

    constructor(private renderer: Renderer, private ref: ElementRef, private router: Router) {
    }

    ngOnInit() {
        this.config = AdConfig;

        if (this.isMobile() && this.format.match(/^leaderboard-/)) {
            this.format = `mobile-${this.format}`;
        }
    }

    isMobile() {
        return Math.max(document.documentElement.clientWidth, window.innerWidth || 0) < 767;
    }

    ngAfterViewInit() {
        if (!AdConfig.enabled) {
            return;
        }

        initBlocked().subscribe(isBlocked => {
            let type = this.getType();

            this.renderer.setElementClass(this.ref.nativeElement, type, true);
            this.renderer.setElementClass(this.ref.nativeElement, this.format, true);

            if (isBlocked) {
                if ((this.isMobile() && this.format == 'banner-atf') ||
                    (!this.isMobile() && this.format == 'leaderboard-atf')) {
                    this.showWhitelist = true;
                } else {
                    // console.debug(`ggs-ad: removing blocked unit ${this.format}`);
                    this.ref.nativeElement.parentNode.removeChild(this.ref.nativeElement);
                }
                return;
            }

            this.initAdsense();
            this.initDFP();
        });
    }

    private initAdsense() {
        if (this.provider != 'adsense') {
            return;
        }

        let tag = tags['adsense'][this.format]

            if (!tag) {
            console.debug(`ggs-ad: missing tag for ${this.format}`);
            return;
        }

        // adsense specific
        this.renderer.setElementAttribute(this.adElement.nativeElement, 'data-ad-client', 'ca-pub-7097583159505683');
        this.renderer.setElementAttribute(this.adElement.nativeElement, 'data-ad-slot', tag);

        setTimeout(() => {
            let adsense = window['adsbygoogle'] ? window['adsbygoogle'] : null;

            if (!adsense) {
                return;
            }

            try {
                adsense.push({});
            } catch (e) {
                console.debug(e);
            }
        });
    }

    private initDFP() {
        if (this.provider != 'dfp') {
            return;
        }

        let tag = tags['dfp'][this.format]

            if (!tag) {
            console.debug(`ggs-ad: missing tag for ${this.format}`);
            return;
        }

        // adsense specific
        this.renderer.setElementAttribute(this.adElement.nativeElement, 'id', this.format);

        setTimeout(() => {
            let googletag = window['googletag'] || {};
            googletag.cmd = googletag.cmd || [];

            initDFPTags(this.router);

            try {
                googletag.cmd.push(() => googletag.display(this.format));
            } catch (e) {
                console.debug(e);
            }
        });
    }

    private initMonkeyBroker() {
        if (this.provider != 'monkeybroker') {
            return;
        }

        if (!window['MonkeyBroker']) {
            return;
        }

        setTimeout(() => {
            let el = document.querySelector('#' + this.format + ' div');
            window['MonkeyBroker'].adPlacement({sizes: this.validSizes, el: el});
        });
    }

    private getType() {
        return this.format.replace(/-[ab]tf/, '');
    }
}
