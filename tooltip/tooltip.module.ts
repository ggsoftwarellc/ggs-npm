import {NgModule} from '@angular/core';

import {Tooltip} from './tooltip';

@NgModule({
    declarations: [
        Tooltip,
    ],
    exports: [
        Tooltip,
    ],
})
export class TooltipModule {
}