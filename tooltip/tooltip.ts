import {Directive, ElementRef, EventEmitter, Input, NgZone, Output} from '@angular/core';
import {EventManager} from '@angular/platform-browser';
import * as _ from 'lodash';

@Directive({
    selector: '[ggs-tooltip]',
    host: {
        '(press)': 'onPress()',
        '(mouseenter)': 'onMouseEnter($event)',
        '(mouseleave)': 'onMouseLeave()',
        '(mousemove)': 'onMouseMove($event)',
        '(click)': 'onClick($event)',
    }
})
export class Tooltip {
    public static bodyElement: any;
    public static containerElement: any;
    public static isMobile: boolean = false;
    public static templates: any = {};

    public static hide() {
        Tooltip.containerElement.style.display = 'none';
    }

    public static register(name: string, template: string) {
        Tooltip.templates[name] = template;
    }

    public static position(event, opts) {
        let padding = 10;

        let y = event.pageY + padding;
        let x = event.pageX + padding;

        let bodyX =
            Math.max(document.documentElement.clientWidth, window.innerWidth || 0) + document.body.scrollLeft - 40;
        let bodyY =
            Math.max(document.documentElement.clientHeight, window.innerHeight || 0) + document.body.scrollTop - 40;

        let errorY = y + Tooltip.containerElement.offsetHeight > bodyY;
        let errorX = x + Tooltip.containerElement.offsetWidth > bodyX;

        let anchor: any = {
            bottomleft: {y: `${y - Tooltip.containerElement.offsetHeight}px`, x: `${x}px`},
            bottomright: {
                y: `${y - Tooltip.containerElement.offsetHeight}px`,
                x: `${x - Tooltip.containerElement.offsetWidth}px`,
            },
            topleft: {y: `${y}px`, x: `${x}px`},
            topright: {
                y: `${y}px`,
                x: `${x - Tooltip.containerElement.offsetWidth}px`,
            }
        };

        if (opts.anchor) {
            Tooltip.containerElement.style.top = anchor[opts.anchor].y;
            Tooltip.containerElement.style.left = anchor[opts.anchor].x;
        } else {
            if (errorY && !errorX) {
                Tooltip.containerElement.style.top = anchor.bottomleft.y;
                Tooltip.containerElement.style.left = anchor.bottomleft.x;

            } else if (!errorY && errorX) {
                Tooltip.containerElement.style.top = anchor.topright.y;
                Tooltip.containerElement.style.left = anchor.topright.x;

            } else if (errorY && errorX) {
                Tooltip.containerElement.style.top = anchor.bottomright.y;
                Tooltip.containerElement.style.left = anchor.bottomright.x;

            } else {
                Tooltip.containerElement.style.top = anchor.topleft.y;
                Tooltip.containerElement.style.left = anchor.topleft.x;
            }
        }
    }

    public static injectContainer() {
        let container = document.querySelector('div.ggs-tooltip-container') as HTMLElement;
        if (container) {
            Tooltip.containerElement = container
            return;
        }

        // create close button div
        let close = document.createElement('div');
        close.className = 'ggs-tooltip-close';
        close.innerHTML = '<i class="fa fa-times-circle"></i>';

        // container body where ttip html is injected
        let body = document.createElement('div');
        body.className = 'ggs-tooltip-body';

        // create tooltip container with click event to clear self for mobile
        container = document.createElement('div')
        container.className = 'ggs-tooltip-container';
        container.style.display = 'none';
        container.style.position = 'absolute';
        container.addEventListener('click', () => {
            console.debug('click');
            if (!Tooltip.isMobile) {
                return;
            }

            Tooltip.hide();
        });

        container.appendChild(close);
        container.appendChild(body);

        Tooltip.containerElement = container;
        Tooltip.bodyElement = body;

        // append to body
        document.querySelector('body').appendChild(container);

        // create window events
        window.addEventListener('scroll', () => {
            if (!Tooltip.isMobile) {
                return;
            }

            Tooltip.containerElement.style.top = document.body.scrollTop + 'px';
        });

        // add resize listener
        let resize =
            () => {
                let container = Tooltip.containerElement;
                Tooltip.isMobile = Math.max(document.documentElement.clientWidth, window.innerWidth || 0) < 767;

                if (Tooltip.isMobile) {
                    container.style.margin = '0 auto';
                    container.style.left = '50%';
                    container.style.transform = 'translate(-50%, 10px)';
                } else {
                    container.style.margin = '0';
                    container.style.transform = 'none';
                };
            }

        resize();
        window.addEventListener('resize', () => resize);
    }

    @Input('ggs-tooltip')
    opts: any;

    @Output('clickTooltip')
    clickTooltip = new EventEmitter();

    html = '';
    pressed = false;

    constructor(private ngZone: NgZone, private elRef: ElementRef, private em: EventManager) {
    }

    ngOnInit() {
        Tooltip.injectContainer();

        if (this.opts.template) {
            this.html = _.template(Tooltip.templates[this.opts.template])(this.opts.templateData);
        } else {
            this.html = `<div class="ggs-tooltip ggs-default-tooltip">${this.opts.message}</div>`;
        }
    }

    ngOnDestroy() {
        Tooltip.hide();
    }

    onPress() {
        if (!Tooltip.isMobile) {
            return;
        }

        Tooltip.bodyElement.innerHTML = this.html;
        Tooltip.containerElement.style.display = 'block';

        this.pressed = true;
    }

    onClick(event) {
        // if a press was register deny the click that follows to prevent the "ghost click"
        if (this.pressed) {
            this.pressed = false;
            event.stopPropagation();
            event.preventDefault();
            return;
        }

        this.clickTooltip.emit(event);
    }

    onMouseLeave() {
        if (Tooltip.isMobile) {
            return;
        }

        Tooltip.hide();
    }

    onMouseEnter(event) {
        if (Tooltip.isMobile) {
            return;
        }

        Tooltip.position(event, this.opts);
        Tooltip.containerElement.innerHTML = this.html;
        Tooltip.containerElement.style.display = 'block';
    }

    onMouseMove(event) {
        if (Tooltip.isMobile) {
            return;
        }

        Tooltip.position(event, this.opts);
    }
}