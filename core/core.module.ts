import { Seo } from './seo';
import { WindowRef } from './window-ref';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

@NgModule({
    providers: [
        Seo,
        WindowRef,
    ],
    imports: [
        CommonModule,
    ],
})
export class CoreModule {
}
