import { Injectable } from '@angular/core';

const nativeWindow = () => {
    return typeof window !== 'undefined' ? window : undefined;
}

@Injectable()
export class WindowRef {
    nativeWindow;

    constructor() {
        this.nativeWindow = nativeWindow();
    }
}